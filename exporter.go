package main

import (
	"context"
	"flag"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	addr    = flag.String("addr", ":3909", "address to listen on")
	doDebug = flag.Bool("debug", false, "log debug messages")
)

const (
	cgroupsV1Root = "/sys/fs/cgroup/systemd"
	cgroupsV2Root = "/sys/fs/cgroup"
)

func hasCGroupsV2() bool {
	_, err := os.Stat("/sys/fs/cgroup/unified")
	return os.IsNotExist(err)
}

func debug(s string, args ...interface{}) {
	if *doDebug {
		log.Printf(s, args...)
	}
}

type subsystem interface {
	describe(chan<- *prometheus.Desc)
	parse(string, string, string, chan<- prometheus.Metric)
}

// Keep a pre-rendered snapshot of all the metrics, so that scraping
// and updates can be independent of each other (but still serve a
// coherent view of all the metrics).
type collector struct {
	subsystems []subsystem
}

func newCollector(subsystems []subsystem) *collector {
	return &collector{
		subsystems: subsystems,
	}
}

func (c *collector) Describe(ch chan<- *prometheus.Desc) {
	for _, s := range c.subsystems {
		s.describe(ch)
	}
}

func (c *collector) Collect(ch chan<- prometheus.Metric) {
	count := 0
	for _, cgroup := range getCGroups() {
		slice, unit := splitServiceName(cgroup)
		for _, s := range c.subsystems {
			s.parse(cgroup, slice, unit, ch)
		}
		count++
	}
	debug("collected metrics from %d cgroups", count)
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Set the right analyzers, and the root path for cgroup
	// scanning, depending on whether we detect cgroups v1 or v2.
	var subsystems []subsystem
	if hasCGroupsV2() {
		log.Printf("cgroups v2 detected")
		subsystems = []subsystem{
			&memoryV2Parser{},
			&cpuV2Parser{},
			&blkioV2Parser{},
		}
		cgroupsRootPath = cgroupsV2Root
	} else {
		log.Printf("cgroups v1 detected")
		subsystems = []subsystem{
			&memoryParser{},
			&cpuParser{},
			&blkioParser{},
		}
		cgroupsRootPath = cgroupsV1Root
	}

	c := newCollector(subsystems)
	reg := prometheus.NewRegistry()
	reg.MustRegister(c)

	// Create a cancelable Context and cancel it when we receive a
	// termination signal. This will stop the metrics updater.
	ctx, cancel := context.WithCancel(context.Background())

	// Run a goroutine that updates periodically the list of
	// cgroups, independently from the Prometheus scrape
	// frequency.
	updateCGroupsList(ctx)

	// Create a very simple HTTP server that only exposes the
	// Prometheus metrics handler.
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			// nolint: errcheck
			io.WriteString(w, `<html>
<body><h1>cgroups-exporter</h1><p><a href=\"/metrics\">/metrics</a></p>
</body>`)
		} else {
			http.NotFound(w, r)
		}
	})

	// Set up the Listener separately to work around Go 1.7's lack
	// of the http.Server.Close() function.
	l, err := net.Listen("tcp", *addr)
	if err != nil {
		log.Fatal(err)
	}

	srv := &http.Server{
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   20 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		cancel()
		l.Close()
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	log.Fatal(srv.Serve(l))
}
