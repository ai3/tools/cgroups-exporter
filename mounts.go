package main

import (
	"log"
	"strings"

	"github.com/moby/sys/mountinfo"
)

var mountedDevices map[int]struct{}

func isDeviceMounted(maj, min int) bool {
	// Fail open.
	if mountedDevices == nil {
		return true
	}

	key := maj<<8 + min
	_, ok := mountedDevices[key]
	return ok
}

func getMountedDevices() (map[int]struct{}, error) {
	// Look only at the mounts whose source starts with /dev/.
	mounts, err := mountinfo.GetMounts(func(info *mountinfo.Info) (bool, bool) {
		return !strings.HasPrefix(info.Source, "/dev/"), false
	})
	if err != nil {
		return nil, err
	}
	out := make(map[int]struct{})
	for _, m := range mounts {
		key := m.Major<<8 + m.Minor
		out[key] = struct{}{}
	}
	return out, nil
}

func init() {
	var err error
	mountedDevices, err = getMountedDevices()
	if err != nil {
		log.Printf("warning: could not read list of mounted devices: %v", err)
	}
}
