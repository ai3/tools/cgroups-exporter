package main

import (
	"bufio"
	"bytes"
	"errors"
	"os"
	"path/filepath"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	ioV2BytesDesc = prometheus.NewDesc(
		"cgroup_blkio_bytes",
		"Bytes read/written by blkio.",
		[]string{"mode", "slice", "service"},
		nil,
	)
	ioV2OpsDesc = prometheus.NewDesc(
		"cgroup_blkio_ops",
		"I/O operations.",
		[]string{"mode", "slice", "service"},
		nil,
	)
	ioV2PressureStalledDesc = prometheus.NewDesc(
		"cgroup_blkio_pressure_stalled_seconds_total",
		"PSI stalled I/O seconds.",
		[]string{"slice", "service"},
		nil,
	)
	ioV2PressureWaitingDesc = prometheus.NewDesc(
		"cgroup_blkio_pressure_waiting_seconds_total",
		"PSI waiting I/O seconds.",
		[]string{"slice", "service"},
		nil,
	)
)

type blkioV2Parser struct{}

func parseDevice(token []byte) (int, int, error) {
	parts := bytes.Split(token, []byte(":"))
	if len(parts) != 2 {
		return 0, 0, errors.New("device not in major:minor format")
	}
	maj, err := strconv.Atoi(string(parts[0]))
	if err != nil {
		return 0, 0, err
	}
	min, err := strconv.Atoi(string(parts[1]))
	if err != nil {
		return 0, 0, err
	}
	return maj, min, nil
}

func parseKVPair(token []byte) (string, int64, error) {
	kvp := bytes.SplitN(token, []byte("="), 2)
	if len(kvp) != 2 {
		return "", 0, errors.New("not an assignment")
	}
	value, err := strconv.ParseInt(string(kvp[1]), 10, 64)
	if err != nil {
		return "", 0, err
	}
	return string(kvp[0]), value, nil
}

func parseKVPairs(tokens [][]byte, out map[string]int64) {
	for _, token := range tokens {
		if key, value, err := parseKVPair(token); err == nil {
			out[key] += value
		}
	}
}

func parseIOV2File(path string) (map[string]int64, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// Sum I/O counters across devices, but only for mounted devices,
	// so we do not count I/O operations twice with LVM/MD.
	result := make(map[string]int64)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Bytes()
		parts := bytes.Split(line, []byte(" "))
		if len(parts) < 2 {
			continue
		}
		maj, min, err := parseDevice(parts[0])
		if err != nil {
			continue
		}
		if !isDeviceMounted(maj, min) {
			continue
		}
		parseKVPairs(parts[1:], result)
	}

	return result, scanner.Err()
}

func (p *blkioV2Parser) describe(ch chan<- *prometheus.Desc) {
	ch <- ioV2BytesDesc
	ch <- ioV2OpsDesc
	ch <- ioV2PressureStalledDesc
	ch <- ioV2PressureWaitingDesc
}

func (p *blkioV2Parser) parse(path, slice, unit string, ch chan<- prometheus.Metric) {
	counters, err := parseIOV2File(filepath.Join(cgroupsRootPath, path, "io.stat"))
	if err != nil {
		return
	}

	ch <- prometheus.MustNewConstMetric(
		ioV2BytesDesc,
		prometheus.CounterValue,
		float64(counters["wbytes"]),
		"write", slice, unit,
	)
	ch <- prometheus.MustNewConstMetric(
		ioV2BytesDesc,
		prometheus.CounterValue,
		float64(counters["rbytes"]),
		"read", slice, unit,
	)
	ch <- prometheus.MustNewConstMetric(
		ioV2OpsDesc,
		prometheus.CounterValue,
		float64(counters["wios"]),
		"write", slice, unit,
	)
	ch <- prometheus.MustNewConstMetric(
		ioV2OpsDesc,
		prometheus.CounterValue,
		float64(counters["rios"]),
		"read", slice, unit,
	)

	waiting, stalled, err := parsePressureFile(filepath.Join(cgroupsRootPath, path, "io.pressure"))
	if err == nil {
		ch <- prometheus.MustNewConstMetric(
			ioV2PressureWaitingDesc,
			prometheus.CounterValue,
			float64(waiting)/usecs,
			slice, unit,
		)
		ch <- prometheus.MustNewConstMetric(
			ioV2PressureStalledDesc,
			prometheus.CounterValue,
			float64(stalled)/usecs,
			slice, unit,
		)
	}
}
