package main

import (
	"bufio"
	"bytes"
	"os"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

func parseBlkioMapFile(path string) (map[string]int64, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// Aggregate counts by operation type (sum by device).
	result := make(map[string]int64)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Bytes()
		parts := bytes.Split(line, []byte(" "))
		if len(parts) != 3 {
			continue
		}
		value, err := strconv.ParseInt(string(parts[2]), 10, 64)
		if err != nil {
			continue
		}
		result[string(parts[1])] += value
	}

	return result, scanner.Err()
}

var (
	ioV1BytesDesc = prometheus.NewDesc(
		"cgroup_blkio_bytes",
		"Bytes read/written by blkio.",
		[]string{"mode", "slice", "service"},
		nil,
	)
	ioV1LatencyDesc = prometheus.NewDesc(
		"cgroup_blkio_latency_ns",
		"Average blkio operation latency (in nanoseconds).",
		[]string{"mode", "slice", "service"},
		nil,
	)
)

type blkioParser struct{}

func (p *blkioParser) describe(ch chan<- *prometheus.Desc) {
	ch <- ioV1BytesDesc
	ch <- ioV1LatencyDesc
}

func (p *blkioParser) parse(path, slice, unit string, ch chan<- prometheus.Metric) {
	ops, err := parseBlkioMapFile(cgroupV1StatPath(path, "blkio", "blkio.io_serviced"))
	if err != nil {
		return
	}
	times, err := parseBlkioMapFile(cgroupV1StatPath(path, "blkio", "blkio.io_service_time"))
	if err != nil {
		return
	}
	totBytes, err := parseBlkioMapFile(cgroupV1StatPath(path, "blkio", "blkio.io_service_bytes"))
	if err != nil {
		return
	}

	ch <- prometheus.MustNewConstMetric(
		ioV1BytesDesc,
		prometheus.CounterValue,
		float64(totBytes["Write"]),
		"write", slice, unit,
	)
	ch <- prometheus.MustNewConstMetric(
		ioV1BytesDesc,
		prometheus.CounterValue,
		float64(totBytes["Read"]),
		"read", slice, unit,
	)

	// This is unfortunately an average.
	if ops["Write"] > 0 {
		ch <- prometheus.MustNewConstMetric(
			ioV1LatencyDesc,
			prometheus.GaugeValue,
			float64(times["Write"])/float64(ops["Write"]),
			"write", slice, unit,
		)
	}
	if ops["Read"] > 0 {
		ch <- prometheus.MustNewConstMetric(
			ioV1LatencyDesc,
			prometheus.GaugeValue,
			float64(times["Read"])/float64(ops["Read"]),
			"read", slice, unit,
		)
	}
}
