module git.autistici.org/ai3/tools/cgroups-exporter

go 1.15

require (
	github.com/golang/protobuf v1.3.2-0.20190409050943-e91709a02e0e // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.2-0.20181231171920-c182affec369 // indirect
	github.com/moby/sys/mountinfo v0.7.2
	github.com/prometheus/client_golang v0.9.3-0.20190412003733-5a3ec6a883d3
	github.com/prometheus/client_model v0.0.0-20190129233127-fd36f4220a90 // indirect
	github.com/prometheus/common v0.3.0 // indirect
	github.com/prometheus/procfs v0.0.0-20190403104016-ea9eea638872 // indirect
	github.com/tklauser/go-sysconf v0.3.14
)
