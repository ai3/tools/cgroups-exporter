package main

import (
	"path/filepath"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	memV2PressureStalledDesc = prometheus.NewDesc(
		"cgroup_mem_pressure_stalled_seconds_total",
		"PSI stalled memory seconds.",
		[]string{"slice", "service"},
		nil,
	)
	memV2PressureWaitingDesc = prometheus.NewDesc(
		"cgroup_mem_pressure_waiting_seconds_total",
		"PSI waiting memory seconds.",
		[]string{"slice", "service"},
		nil,
	)
)

type memoryV2Parser struct{}

func (p *memoryV2Parser) describe(ch chan<- *prometheus.Desc) {
	ch <- memV1Desc
	ch <- memV2PressureStalledDesc
	ch <- memV2PressureWaitingDesc
}

func (p *memoryV2Parser) parse(path, slice, unit string, ch chan<- prometheus.Metric) {
	rss, err := parseSingleValueFile(filepath.Join(cgroupsRootPath, path, "memory.current"))
	if err != nil {
		debug("error parsing memory.current: %v", err)
		return
	}

	ch <- prometheus.MustNewConstMetric(
		memV1Desc,
		prometheus.GaugeValue,
		float64(rss),
		slice, unit,
	)

	waiting, stalled, err := parsePressureFile(filepath.Join(cgroupsRootPath, path, "memory.pressure"))
	if err == nil {
		ch <- prometheus.MustNewConstMetric(
			memV2PressureWaitingDesc,
			prometheus.CounterValue,
			float64(waiting)/usecs,
			slice, unit,
		)
		ch <- prometheus.MustNewConstMetric(
			memV2PressureStalledDesc,
			prometheus.CounterValue,
			float64(stalled)/usecs,
			slice, unit,
		)
	}
}
