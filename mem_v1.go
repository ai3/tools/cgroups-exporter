package main

import (
	"github.com/prometheus/client_golang/prometheus"
)

var memV1Desc = prometheus.NewDesc(
	"cgroup_memory_usage",
	"Cgroup memory usage (RSS, in bytes).",
	[]string{"slice", "service"},
	nil,
)

type memoryParser struct{}

func (p *memoryParser) describe(ch chan<- *prometheus.Desc) {
	ch <- memV1Desc
}

func (p *memoryParser) parse(path, slice, unit string, ch chan<- prometheus.Metric) {
	mstat, err := parseMapFile(cgroupV1StatPath(path, "memory", "memory.stat"))
	if err != nil {
		debug("error parsing memory.stat: %v", err)
		return
	}

	ch <- prometheus.MustNewConstMetric(
		memV1Desc,
		prometheus.GaugeValue,
		float64(mstat["total_rss"]),
		slice, unit,
	)
}
