package main

import (
	"bufio"
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	sysconf "github.com/tklauser/go-sysconf"
)

var (
	userHZ float64
)

func init() {
	// Cgroups v1 counters are expressed in 'ticks', so we need to figure
	// out the system's HZ value to convert them to seconds.
	userHZ = 100
	if clktck, err := sysconf.Sysconf(sysconf.SC_CLK_TCK); err == nil {
		userHZ = float64(clktck)
	}
}

func cgroupV1StatPath(cgroupPath, collector, path string) string {
	return filepath.Join("/sys/fs/cgroup", collector, cgroupPath, path)
}

// Parse a generic proc-style 'map' file, with space-separated "key value"
// assignments, one per line.
func parseMapFile(path string) (map[string]int64, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	result := make(map[string]int64)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Bytes()
		parts := bytes.Split(line, []byte(" "))
		if len(parts) != 2 {
			continue
		}
		value, err := strconv.ParseInt(string(parts[1]), 10, 64)
		if err != nil {
			continue
		}
		result[string(parts[0])] = value
	}
	return result, scanner.Err()
}

// Parse a file containing a single integer value.
func parseSingleValueFile(path string) (int64, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return 0, err
	}
	if n := bytes.IndexByte(data, '\n'); n >= 0 {
		data = data[:n]
	}
	return strconv.ParseInt(string(data), 10, 64)
}

// Parse a PSI /proc file and return the "some" (waiting), "full" (stalled)
// counters.
func parsePressureFile(path string) (int64, int64, error) {
	f, err := os.Open(path)
	if err != nil {
		return 0, 0, err
	}
	defer f.Close()

	var waiting, stalled int64
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Bytes()
		parts := bytes.Split(line, []byte(" "))
		if len(parts) != 5 {
			continue
		}
		_, value, err := parseKVPair(parts[4])
		if err != nil {
			continue
		}
		switch {
		case bytes.Equal(parts[0], []byte("some")):
			waiting = value
		case bytes.Equal(parts[0], []byte("full")):
			stalled = value
		}
	}
	return waiting, stalled, scanner.Err()
}

func splitServiceName(path string) (string, string) {
	slice, name := filepath.Split(path)
	slice = strings.Trim(slice, "/")
	return slice, name
}
