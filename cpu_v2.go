package main

import (
	"path/filepath"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	usecs float64 = 1000000

	cpuV2PressureStalledDesc = prometheus.NewDesc(
		"cgroup_cpu_pressure_stalled_seconds_total",
		"PSI stalled CPU seconds.",
		[]string{"slice", "service"},
		nil,
	)
	cpuV2PressureWaitingDesc = prometheus.NewDesc(
		"cgroup_cpu_pressure_waiting_seconds_total",
		"PSI waiting CPU seconds.",
		[]string{"slice", "service"},
		nil,
	)
)

type cpuV2Parser struct{}

func (p *cpuV2Parser) describe(ch chan<- *prometheus.Desc) {
	ch <- cpuV1Desc
	ch <- cpuV2PressureStalledDesc
	ch <- cpuV2PressureWaitingDesc
}

func (p *cpuV2Parser) parse(path, slice, unit string, ch chan<- prometheus.Metric) {
	usage, err := parseMapFile(filepath.Join(cgroupsRootPath, path, "cpu.stat"))
	if err != nil {
		debug("error parsing cpu.stat: %v", err)
		return
	}

	ch <- prometheus.MustNewConstMetric(
		cpuV1Desc,
		prometheus.GaugeValue,
		float64(usage["user_usec"])/usecs,
		"user", slice, unit,
	)
	ch <- prometheus.MustNewConstMetric(
		cpuV1Desc,
		prometheus.GaugeValue,
		float64(usage["system_usec"])/usecs,
		"system", slice, unit,
	)

	waiting, stalled, err := parsePressureFile(filepath.Join(cgroupsRootPath, path, "cpu.pressure"))
	if err == nil {
		ch <- prometheus.MustNewConstMetric(
			cpuV2PressureWaitingDesc,
			prometheus.CounterValue,
			float64(waiting)/usecs,
			slice, unit,
		)
		ch <- prometheus.MustNewConstMetric(
			cpuV2PressureStalledDesc,
			prometheus.CounterValue,
			float64(stalled)/usecs,
			slice, unit,
		)
	}
}
