package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"
)

var (
	cgroupsUpdateInterval = flag.Duration("cgroups-update-interval", 60*time.Second, "update interval")
	cgroupsFilterRx       = regexpFlag{Regexp: regexp.MustCompile("^system.slice/")}

	cgroupsRootPath string
)

func init() {
	flag.Var(&cgroupsFilterRx, "cgroups-filter", "filter for cgroup paths")
}

type regexpFlag struct {
	*regexp.Regexp
}

func (f *regexpFlag) Set(value string) error {
	rx, err := regexp.Compile(value)
	if err != nil {
		return err
	}
	f.Regexp = rx
	return nil
}

func (f *regexpFlag) String() string {
	if f.Regexp == nil {
		return "<nil>"
	}
	return fmt.Sprintf("\"%s\"", f.Regexp.String())
}

func findCGroups() ([]string, error) {
	var paths []string
	err := filepath.Walk(cgroupsRootPath, func(path string, info os.FileInfo, err error) error {
		if err != nil || !info.IsDir() || !strings.HasSuffix(path, ".service") {
			return nil
		}
		// Do not track systemd internal services.
		if strings.HasPrefix(info.Name(), "systemd-") {
			return nil
		}
		relPath := path[len(cgroupsRootPath)+1:]
		if !cgroupsFilterRx.MatchString(relPath) {
			return nil
		}
		paths = append(paths, relPath)
		return nil
	})
	debug("found %d cgroups below %s", len(paths), cgroupsRootPath)
	return paths, err
}

var (
	// The lock is mostly there to ensure core-coherence, we're
	// only ever replacing the slice, not touching the back-end
	// array.
	cgroupsMx  sync.Mutex
	curCGroups []string
)

func getCGroups() []string {
	cgroupsMx.Lock()
	defer cgroupsMx.Unlock()
	return curCGroups
}

func updateCGroupsList(ctx context.Context) {
	updateFn := func() {
		cgroups, err := findCGroups()
		if err != nil {
			log.Printf("error scanning /sys/fs/cgroup: %v", err)
			return
		}
		cgroupsMx.Lock()
		curCGroups = cgroups
		cgroupsMx.Unlock()
	}

	updateFn()

	go func() {
		ticker := time.NewTicker(*cgroupsUpdateInterval)
		defer ticker.Stop()
		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				updateFn()
			}
		}
	}()
}
