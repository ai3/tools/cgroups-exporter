package main

import (
	"github.com/prometheus/client_golang/prometheus"
)

var cpuV1Desc = prometheus.NewDesc(
	"cgroup_cpu_usage",
	"Cgroup CPU usage.",
	[]string{"mode", "slice", "service"},
	nil,
)

type cpuParser struct{}

func (p *cpuParser) describe(ch chan<- *prometheus.Desc) {
	ch <- cpuV1Desc
}

func (p *cpuParser) parse(path, slice, unit string, ch chan<- prometheus.Metric) {
	usage, err := parseMapFile(cgroupV1StatPath(path, "cpu,cpuacct", "cpuacct.stat"))
	if err != nil {
		debug("error parsing cpuacct.stat: %v", err)
		return
	}

	ch <- prometheus.MustNewConstMetric(
		cpuV1Desc,
		prometheus.GaugeValue,
		float64(usage["user"])/userHZ,
		"user", slice, unit,
	)
	ch <- prometheus.MustNewConstMetric(
		cpuV1Desc,
		prometheus.GaugeValue,
		float64(usage["system"])/userHZ,
		"system", slice, unit,
	)
}
